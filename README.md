 # WindFarmDemo Web application
This application is dependent on the REST service [windfarm demo](https://github.com/hudikm/WindFarmDemo).

[Documentation(Slovak)](https://hudikm.github.io/WindfarmDemoDocs/)

How to start the WindFarmDemo application
---

1. Run `mvn clean install` to build your application
1. Start application with `java -jar target/WindFarmDemo-1.0-SNAPSHOT.jar server config.yml`
1. To check that your application is running enter url `http://localhost:8080`

Health Check
---

To see your applications health enter url `http://localhost:8081/healthcheck`


Semestálna práca pozostáva s troch časti:

1. WindFarmDemoWeb
2. WindFarmDemo
3. Weather

WindFarmDemoWeb

Táto časť sa skladá z webového rozhrania, ktorá komunikuje s back-endom(WindFarmDemo). Pre zobrazenie údajov počasia sme vytvorili nový ftl súbor. V tomto ftl súbore sme si zadefinovali rozloženie a
vzhľad stránky. Následne sme si vňom namapovali jednotlivé atribúty tried do polí. Ďalej sme vytvorili objekty DevicesView a WeatherView, ktoré sa namapovali. V časti resource sme vytvorili prílušné
requesty na back-end. Aby bol request úspšne vykonaný používa sa autentifikačný token. Pomocou tohto tokenu sa autorizuje reguest na back-end a následne ak je všetko v poriadku bude mu povolený prítup.
Následne mu budú poskytnuté údaje s databázy ktoré sa zobrazia na webovej stránke.

WindFarmDemo

Táto časť predstavu back-end, ktorý poskytuje dáta pre našu prvú časť WindFarmDemoWeb. Ďalej v tomto projekte sme doimplemetovali triedy pre naše zariadenie respektíve mesto ako sú Device.class, 
BasicDev_Authentificator.class, OpenWeatherOBJ.claass a mnoho daľších. Základnou triedou je trieda Device.class , ktorá predstavuje parametre pre naše zariadenie ako je meno, heslo a rola pre autorizáciu.
BasicDev_Auhtorizer.class a BasicDev_Authentificator.class obsahujú metódy na autorizáciu a autentifikáciu zariadení tieto triedy vstupujú do AutjFiltra pri inicializácií jersey.
Dev_Resource.class je trieda, pomocou ktorej jersey roztriedi jednotlivé resource a vykoná príslušnú obsluhu volania.
Nakoniec sme implementovali triedy pre počasie ako základ pre prácu s databázou. Tieto tredy sa nachádzú v package sk.fri.uniza.openweathermap. Pre prácu s databázou sa používajú konkrétne tieto dve triedy:
LiteWeatherOBJ.class a OpenWeatherOBJ.class.

Weather

Weather projekt nám emuluje to naše zariadenie, ktoré nám získavajú dáta o počasí. Tieto dáta sú získavané zo servera OpenWeather web pomocou restového rozhrania. Tieto údaje sú aktuálne s reálneho časú a 
následne sú odosielané na back-end (WindFarmDemo). Komunikácia medzi Weather a WindFarmDemo je pomocou restového rozhrania. Na získanie údajov pre počasie sme si museli vytvorit na stránke OpenWeather.com 
svoj vlastný API klúč na základe ktorého nám boli poskytované príslušné údaje. Tieto údaje Meno, Heslo a API sú v triede Config.class. Zariadenie je autorizované pomocou BasicAuthentification, to znamená že
meno a heslo je vkladané priamo do hlavičky HTTP protokolu zakódované v Base64. Údaje sú aktualizované každých 60 sekúnd.
Aplikácia ďalej obsahuje triedy Dev_Resource a DevZ_Resource. Tieto triedy nám slúžia na autentifikáciu zariadenia, komunikáciu s openweather a komunikáciu s back-endom.

