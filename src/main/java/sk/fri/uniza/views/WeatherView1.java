package sk.fri.uniza.views;

import sk.fri.uniza.api.LiteWeatherOBJ;
import sk.fri.uniza.core.User;

import javax.ws.rs.core.UriInfo;
import java.util.List;

public class WeatherView1 extends MaterializePage<MaterializeHeader, MaterializeFooter> {
    private final User loginUser;
    private final String toastMsg;
    private LiteWeatherOBJ Wobj;
    private List<LiteWeatherOBJ> w;

    public WeatherView1(UriInfo uriInfo, User loginUser, List<LiteWeatherOBJ> Wobj, String toastMsg) {
        super("weather.ftl", uriInfo, new MaterializeHeader(loginUser, "Weather", true), new MaterializeFooter());
        this.loginUser = loginUser;
        this.toastMsg = toastMsg;
        this.w = Wobj;

    }

    public String getToastMsg() {
        return toastMsg;
    }

    public User getLoginUser() {
        return loginUser;
    }

    public List<LiteWeatherOBJ> getWobj() {
        return w;
    }
}
