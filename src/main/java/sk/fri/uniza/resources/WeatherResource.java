package sk.fri.uniza.resources;

import io.dropwizard.auth.Auth;
import retrofit2.Response;
import sk.fri.uniza.auth.Role;
import sk.fri.uniza.auth.Session;
import sk.fri.uniza.core.User;
import sk.fri.uniza.views.WeatherView;
import sk.fri.uniza.views.WeatherView1;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import java.io.IOException;

@Path("/weather")
public class WeatherResource {

    @GET
    @Path("Púchov")
    @Produces(MediaType.TEXT_HTML)
    @RolesAllowed({Role.USER_READ_ONLY, Role.ADMIN})
    public WeatherView getPersonInfo(@Auth User user, @Context UriInfo uriInfo, @Context HttpHeaders headers) {

        return null;

    }

    @GET
    @Path("Žilina")
    @Produces(MediaType.TEXT_HTML)
    @RolesAllowed({Role.USER_READ_ONLY, Role.ADMIN})
    public WeatherView1 getPersonInfo() {

        return null;

    }



}
